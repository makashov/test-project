<?php

const CONFIGS =  [
    // Настройки подключения к БД
    'db_host' => 'localhost',
    'db_user' => 'root',
    'db_password' => '',
    'db_name' => 'test_project',

    //Таблица данных в БД
    'data_table' => 'data',

    //Путь к csv файлу
    'csv_path' => '../data.csv',

];