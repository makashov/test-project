<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>Статус записи <?= $random_row->id ?> был изменён на <?= $random_row->status ?></h1>

<table>
    <tr>
        <th>ID акции</th>
        <th>Название акции</th>
        <th>Ссылка</th>
    </tr>
    <?php foreach($all_data as $item): ?>
        <?php /** @var Lib\DataRow $item */ ?>
        <tr>
            <td><?= $item->id ?></td>
            <td><?= $item->name ?></td>
            <td><a href="#"><?= $item->getUrl() ?></a></td>
        </tr>
    <?php endforeach; ?>
</table>

</body>
</html>