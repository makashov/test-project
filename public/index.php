<?php

include '../config.php';

spl_autoload_register(function ($class_name){
    $path = dirname(__DIR__) .DIRECTORY_SEPARATOR. $class_name . '.php';
    $path = str_replace('\\', '/', $path);
    include($path);
});

$query_builder = new \Database\Builders\MySqlBuilder();

$parser = new Lib\CsvParser(CONFIGS['csv_path']);

// Конвертируем заданные поля в необходимые типы
$parser->parseContents()->convertData([
    'data-okonchaniya' => function($data) {
        return \DateTime::createFromFormat('d-m-Y', $data)->format('Y-m-d');
    },
    'data-nachala-aktsii' => function($data) {
        return \DateTime::createFromFormat('d-m-Y', $data)->getTimestamp();
    }
]);

$parsed_Data = $parser->getContents();

if( !$query_builder->isTableExists(CONFIGS['data_table']) ) {
    $query_builder->createTable(CONFIGS['data_table'], [
        'id' => 'primary',
        'name' => 'text',
        'start_date' => 'integer',
        'end_date' => 'date',
        'status' => 'text'
    ]);

    $query_builder->setTable(CONFIGS['data_table'])->bulkInsert(['id', 'name', 'start_date', 'end_date', 'status'], $parsed_Data);

    $random_row = $query_builder
        ->setTable(CONFIGS['data_table'])
        ->select(['id', 'status'])
        ->limit(1)
        ->inRandomOrder()
        ->first();

    $random_row->status = $random_row->status == 'Off' ? 'On' : 'Off';

    $query_builder = new \Database\Builders\MySqlBuilder();
    $query_builder->setTable(CONFIGS['data_table'])
        ->where(['id' => $random_row->id])
        ->update(['status' => $random_row->status]);

    $all_data = (new \Database\Builders\MySqlBuilder())->setTable(CONFIGS['data_table'])->get();

    extract([$random_row, $all_data]);

    include '../views/index.php';
}
else{
    exit('Таблица "'. CONFIGS['data_table'] . '" уже сущестувет.');
}
