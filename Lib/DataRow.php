<?php

namespace Lib;

class DataRow
{
    use Transliterator;

    public $id;
    public $name;
    public $start_date;
    public $end_date;
    public $status;

    public function getUrl()
    {
        return $this->convertToLatin($this->id) . '-' . $this->convertToLatin($this->name);
    }
}