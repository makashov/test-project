<?php

namespace Lib;


class CsvParser
{
    use Transliterator;

    /** @var  string $path Путь к csv файлу */
    protected $path;

    /** @var array $contents содержимое файла */
    protected $contents = [];

    /** @var  array $titles Загаловки файла */
    protected $titles = [];

    /**
     * @var array $latinTitles Загаловки на латинице
     */
    protected $latinTitles = [];

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function convertData(array $data_types)
    {
        foreach ($data_types as $data => $closure){
            $key = array_search($data, $this->latinTitles);
            foreach ($this->contents as &$content) {
                $value = $closure($content[$key]);
                $content[$key] = $value;
            }
        }

        return $this;
    }

    /**
     * Парсинг данных из файла
     * @return $this
     */
    public function parseContents()
    {
        $resource = fopen($this->path, 'r');
        $this->titles = fgetcsv($resource, 0, ';');
        foreach ($this->titles as $title) {
            // Конвертируем название загаловков на латинские символы
            $this->latinTitles[] = $this->convertToLatin($title);
        }

        while( $data = fgetcsv($resource, 0, ';') ) {
            $this->contents []= $data;
        }

        fclose($resource);

        return $this;
    }

    public function getContents()
    {
        return $this->contents;
    }
}