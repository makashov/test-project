<?php

namespace Lib;

trait Transliterator {

    public $dictionary = [
        "а" => "a",
        "ый" => "iy",
        "ые" => "ie",
        "б" => "b",
        "в" => "v",
        "г" => "g",
        "д" => "d",
        "е" => "e",
        "ё" => "yo",
        "ж" => "zh",
        "з" => "z",
        "и" => "i",
        "й" => "y",
        "к" => "k",
        "л" => "l",
        "м" => "m",
        "н" => "n",
        "о" => "o",
        "п" => "p",
        "р" => "r",
        "с" => "s",
        "т" => "t",
        "у" => "u",
        "ф" => "f",
        "х" => "kh",
        "ц" => "ts",
        "ч" => "ch",
        "ш" => "sh",
        "щ" => "shch",
        "ь" => "",
        "ы" => "y",
        "ъ" => "",
        "э" => "e",
        "ю" => "yu",
        "я" => "ya",
        "йо" => "yo",
        "ї" => "yi",
        "і" => "i",
        "є" => "ye",
        "ґ" => "g"
    ];

    public function convertToLatin($string){
        //строка формируется в нижнем регистре
        $string = mb_strtolower($string);

        $string = strtr($string, $this->dictionary);

        //все знаки препинания и пробелы заменяются на "-"
        $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);

        //2 и больше "-" подряд преобразовываются в один
        $string = preg_replace("/-{2,}/", "-", $string);

        return $string;
    }
}